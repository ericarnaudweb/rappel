import {Component} from '@angular/core';
import {ActionSheetController, AlertController, DateTime, NavController} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {DatePipe} from "@angular/common";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    dateJour = new Date().toISOString();

    data: any;
    constructor(private datePipe : DatePipe, private storage: Storage,public navCtrl: NavController,public alertCtrl: AlertController) {
        this.dateJour = this.datePipe.transform(this.dateJour, 'yyyy-MM-dd');
        console.log(this.dateJour);
        this.viewRappelOfDay()
    }

    viewRappelOfDay() {
        this.storage.get('rappel').then(value => {
                this.data = value;
                console.log(value);
        });
    }

    alarmSwitch() {
        let alert = this.alertCtrl.create({
            title: "Confirmez vous l'annulation du rappel",
            message: 'Etes vous sur ?',
            buttons:[
                {
                    text:'Valider',
                    role:'validate',
                },
            ]
        })
        alert.present();
    }
}
