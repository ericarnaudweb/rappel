import {Component} from '@angular/core';
import {ActionSheetController, AlertController, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";

/**
 * Generated class for the NotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-note',
    templateUrl: 'note.html',
})
export class NotePage {
    id: any;
    data: any;
    title: any;
    description: any;
    date: any;
    displayForm: boolean;

    constructor(public alertCtrl: AlertController,public actionSheetCtrl: ActionSheetController, private storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
        // this.storage.clear();
        this.displayForm = false;

    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad NotePage');
    }

    ionViewWillEnter() {
        this.storage.get('rappel').then(value => {
            this.data = value;
            console.log(this.data);
        });
    }

    // Les perametres peuvent avoir des noms different. Seul leur position est importante
    detail(id, li) {
        console.log(id);
        console.log(li);
        const actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: 'Editer',
                    role: 'edit',
                    handler: () => {
                        this.displayForm = true;
                        this.id = id;
                        console.log(this.id);
                        this.title = li.title;
                        this.description = li.description;
                        this.date = li.date;
                        console.log('Edition effectuée');
                    }
                },
                {
                    text: 'Supprimer',
                    role: 'destructive',
                    handler: () => {
                        console.log('Note supprimée');
                        this.delete(id);
                    }
                },
                {
                    text: 'Fermer',
                    role: 'cancel',
                    handler: () => {
                        console.log('Fermeture demandée');

                    }
                }
            ]
        });
        actionSheet.present();
    }

    private delete(id: any) {
        let alert = this.alertCtrl.create({
            title: 'Confirmez vous la suppression',
            message: 'Etes vous sur ?',
            buttons:[
                {
                    text:'Cancel',
                    role:'cancel',
                    handler: ()=> {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text:'Valider',
                    handler: ()=> {
                        console.log('Cancel clicked');
                        this.data.splice(id,1);
                        this.storage.set('rappel',this.data);
                    }
                },
            ]
        })
        alert.present();
    }
}
