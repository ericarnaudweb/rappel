import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';

/**
 * Generated class for the AjoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-ajout',
  templateUrl: 'ajout.html',
})
export class AjoutPage {
  displayForm : boolean;
  constructor(public actionSheetCtrl: ActionSheetController, public navCtrl: NavController, public navParams: NavParams) {
    this.displayForm = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AjoutPage');
  }
  openMenu(){
      const actionSheet = this.actionSheetCtrl.create({
          title: 'Ajouter un rappel',
          buttons: [
              {
                  text: 'Ajouter',
                  role: 'add',
                  handler: () => {
                    this.displayForm = true;
                      console.log('Ajout demandé');
                  }
              },
              {
                  text: 'Cancel',
                  role: 'cancel',
                  handler: () => {
                      console.log('Cancel clicked');
                  }
              }
          ]
      });

      actionSheet.present();
  }
}
