import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import {NotePage} from "../note/note";
import {AjoutPage} from "../ajout/ajout";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab4Root = NotePage;
  tab2Root = AjoutPage;

  constructor() {

  }
}
