import {Component, Input} from '@angular/core';
import {Storage} from '@ionic/storage';
import {AlertController, NavController} from 'ionic-angular';
import {AjoutPage} from "../../pages/ajout/ajout";
import {NotePage} from "../../pages/note/note";

/**
 * Generated class for the MyformComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    selector: 'myform',
    templateUrl: 'myform.html',
})
export class MyformComponent {

    @Input() monid: any;
    @Input() madate: any;
    @Input() letitre: string;
    @Input() madescription: string;

    text: string;
    todo = {};
    displayValider : boolean;

    constructor(private storage: Storage, private alertCtrl: AlertController, public navCtrl: NavController) {
        console.log('Hello MyformComponent Component');
        this.text = 'Hello World';
        this.displayValider = true;
    }

    save() {
        this.storage.get('rappel').then(value => {
            if (value) {
                let array = [];
                array = value;
                let taille = value.length;
                array[taille] = this.todo;
                this.storage.set('rappel', array);
            } else {
                this.storage.set('rappel', [this.todo]);
            }
            this.presentAlert();
            this.navCtrl.setRoot(AjoutPage);
        })
    }

    presentAlert() {
        let alert = this.alertCtrl.create({
            title: 'Succès',
            subTitle: 'Votre rappel a bien été ajouté',
            buttons: ['Ok']
        });
        alert.present();
    }

    ngAfterViewInit(){

        setTimeout(()=>{
            console.log(this.letitre);
            if(this.letitre){
                this.todo['title'] = this.letitre;
                this.displayValider = false;
            }
            if(this.madescription){
                this.todo['description'] = this.madescription;
            }
            if(this.madate){
                this.todo['date'] = this.madate;
            }
            console.log(this.madate);
        })

    }

    onEditClick() {
        this.storage.get('rappel').then(value => {
            if (value) {
                let array = [];
                array = value;
                //array.splice(this.monid, 1);
                array[this.monid]= this.todo;
                this.storage.set('rappel',array);
            }

            // this.presentAlert();
            this.navCtrl.setRoot(NotePage);
        })
    }
}
