import { NgModule } from '@angular/core';
import { CustomHeaderComponent } from './custom-header/custom-header';
import { MyformComponent } from './myform/myform';
@NgModule({
	declarations: [CustomHeaderComponent,
    MyformComponent],
	imports: [],
	exports: [CustomHeaderComponent,
    MyformComponent]
})
export class ComponentsModule {}
