import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { NotePage } from '../pages/note/note';
import { CustomHeaderComponent } from '../components/custom-header/custom-header';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AjoutPage} from "../pages/ajout/ajout";
import {MyformComponent} from "../components/myform/myform";
import {IonicStorageModule} from "@ionic/storage";
import {DatePipe} from "@angular/common";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    NotePage,
    AjoutPage,
    CustomHeaderComponent,
    MyformComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    NotePage,
    AjoutPage,
    CustomHeaderComponent,
    MyformComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatePipe,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
